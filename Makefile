ZIP      := zip -x \*.tmx \*.xcf
LOVE     := love

NAME     := ds_alpha
VERSION  := 0.0.0

CURRENTDIR := $(realpath .)
SRCDIR   := src
BUILDDIR := build

.PHONY : all
all : run

.PHONY : docker
docker :
	docker run --env-file=config --volume $(CURRENTDIR):/var/lib/builder/workspace thallian/love-release

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

love : $(BUILDDIR)
	cd $(SRCDIR)/ && \
	$(ZIP) -9 -r ../$(BUILDDIR)/$(NAME)_$(VERSION).love .

run :
	$(LOVE) $(SRCDIR)/

clean :
	rm -rf $(BUILDDIR)/*

kill:
	kill -s kill $(ps | grep "love src/" | awk '{print $1}')
