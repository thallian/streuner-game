[![build status](https://gitlab.com/thallian/streuner-game/badges/master/build.svg)](https://gitlab.com/thallian/streuner-game/commits/master)

# A Game About a Band
And this band is called [Die Streuner](http://www.streuner.de/) ([*The Strays*](https://en.wikipedia.org/wiki/Die_Streuner) in English).  
I am a big fan ever since I've discovered them in 2007 and I had the opportunity to get to know the different people better over the years.

So why not try and create a little game about them and at the same time learn a bit about game development. 
Maybe I'll even manage to create a playable version.

At the moment it is more of a very simple techdemo but I hope to change that in the near future.

In the meantime take a moment to listen to the music of *Die Streuner*:

- [Gotta get it Home (Live)](https://www.youtube.com/watch?v=kNxE7Jxk1DY)
- [Scherenschleiferweise (Knife Grinder's Tune)](https://www.youtube.com/watch?v=Zo7Ckd1Lrmo)

![Screenshot](screenshot.png)

# Usage
Use arrow keys for movement and enter to get rid of dialogs (for movement w, a, s, d & h, j, k, l works too).

# Downloads
You can download the automatic builds from the [Pipeline](https://gitlab.com/thallian/streuner-game/pipelines) section.

# Building
You can either build it locally (mostly for development) or with docker (for distribution).

## Local Building
### Prerequisites
- [Löve](https://love2d.org/)
- make
- zip
- kill
- grep
- awk
- ps

### Usage
```
make run
```

## Docker Building

Uses [docker](https://hub.docker.com/r/thallian/love-release/) with [love-release](https://github.com/MisterDA/love-release) to build the different OS packages.

### Prerequisites
- [Docker](https://www.docker.com/)
- make

### Usage
```
make docker
```

Or you can look up the exact command in the [Makefile](Makefile)

# Licensation
All original code is licensed under the [MPL 2.0](https://www.mozilla.org/MPL/2.0/index.txt).

# Libraries
- [30log](https://github.com/Yonaba/30log): [MIT](http://opensource.org/licenses/mit-license.php)
- [anim8](https://github.com/kikito/anim8): [MIT](http://opensource.org/licenses/mit-license.php)
- [bump](https://github.com/kikito/bump.lua): [MIT](http://opensource.org/licenses/mit-license.php)
- [sti](https://github.com/karai17/Simple-Tiled-Implementation): [MIT](http://opensource.org/licenses/mit-license.php)

The tiles and character images are from the [liberated pixel cup](http://lpc.opengameart.org/) and are under a [cc-by.sa 3.0](http://creativecommons.org/licenses/by-sa/3.0/) license.

# History
I started a first version of this in ~2008 with the [RPG Maker](http://www.rpgmakerweb.com/) (no idea which version), back when I had even less an idea of software development than today :)

The only thing surviving is the following screenshot:

![history](history.jpg)
