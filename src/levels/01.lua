return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "v0.15.1-76-g0cd1368",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 20,
  height = 20,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 7,
  properties = {},
  tilesets = {
    {
      name = "atlas00",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "tiles/atlas00.png",
      imagewidth = 1024,
      imageheight = 1024,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1024,
      tiles = {
        {
          id = 184,
          properties = {
            ["collidable"] = true
          }
        }
      }
    },
    {
      name = "atlas01",
      firstgid = 1025,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "tiles/atlas01.png",
      imagewidth = 1024,
      imageheight = 1024,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1024,
      tiles = {}
    },
    {
      name = "atlas03",
      firstgid = 2049,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "tiles/atlas03.png",
      imagewidth = 1024,
      imageheight = 1024,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1024,
      tiles = {}
    },
    {
      name = "atlas04",
      firstgid = 3073,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "tiles/atlas04.png",
      imagewidth = 1024,
      imageheight = 1024,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1024,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "background",
      x = 0,
      y = 0,
      width = 20,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztzsENgkAQQNEVFC9ShJpAAzYAEq1LglqHoFKHHLQOMNE2/CRLnJOHdQ8cOLzMZLL52WymVGbZDin2hg44it4JOS4o9GydxV78uF1Rit4NFZ6G7niIXo3mj94L76HXm97K6ff/hp7d3shXyoGLidbuYz0leeveeZj6396cfYGloQCh6EXsMdYGEmywFT2bPspTJQc="
    },
    {
      type = "tilelayer",
      name = "foreground00",
      x = 0,
      y = 0,
      width = 20,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztk71OAkEURq+E3QSS3cUX4SdZXgPlFaRVflp4hQW0BRoT7bTkATTSgZS8gJYKhXQWHCpIZoCZhZIvOZlkJvfk3puMyPH5u1Dv/LRIABm4TNv5hhpfFkcO8lCAG0+kAoPUYV+YUO9KOK7gGspb/fUNfLpUcdSgDo0tXy+mr4OjC/fwYLk/m5jsT5cmdS0N/zF960SuSNtVz7h5ofbVVc+4GVM7cdXznE2++JPfnv4tZFdFy30lfRHH3/2+cMw8j/T0BM/wvqM/m7zh+ICRges2ELmDKtRgQs0nTGHGbMs98+kS4WhDB7rwg+cX5ieYax2PfnwIDPpaAcTwMd4="
    },
    {
      type = "tilelayer",
      name = "foreground01",
      x = 0,
      y = 0,
      width = 20,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBiaIJSPuuaVUtm8UTAKRgEquMxKXfM+U9k8XOAeFyo9ChAAANQNBIc="
    },
    {
      type = "objectgroup",
      name = "objects",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      objects = {
        {
          id = 1,
          name = "player",
          type = "",
          shape = "rectangle",
          x = 320,
          y = 480,
          width = 32,
          height = 64,
          rotation = 0,
          visible = true,
          properties = {
            ["collision"] = "player_collision",
            ["has_controller"] = true
          }
        },
        {
          id = 2,
          name = "romata",
          type = "",
          shape = "rectangle",
          x = 320,
          y = 160,
          width = 32,
          height = 64,
          rotation = 0,
          visible = true,
          properties = {
            ["collision"] = "romata_collision",
            ["has_controller"] = true
          }
        },
        {
          id = 3,
          name = "player_collision",
          type = "",
          shape = "rectangle",
          x = 332,
          y = 521,
          width = 8,
          height = 8,
          rotation = 0,
          visible = true,
          properties = {
            ["collidable"] = "true"
          }
        },
        {
          id = 5,
          name = "romata_collision",
          type = "",
          shape = "rectangle",
          x = 328,
          y = 203,
          width = 14,
          height = 18,
          rotation = 0,
          visible = true,
          properties = {
            ["collidable"] = "true"
          }
        }
      }
    },
    {
      type = "tilelayer",
      name = "overlay",
      x = 0,
      y = 0,
      width = 20,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBgFo4C+4Asjdc0TYaKueaMAAhQ4GRgSOAfaFaNgMAMALD4Bng=="
    },
    {
      type = "tilelayer",
      name = "collision",
      x = 0,
      y = 0,
      width = 20,
      height = 20,
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {
        ["collidable"] = "true"
      },
      encoding = "base64",
      compression = "zlib",
      data = "eJzbycDAsHOIY0KAFHOIMY8YM5HVUdM8YgGx+qnlPlLdSqn/aGleLuPgdt9gNo9Q3iLVbGqZR0maxVY2UKPMolb5RwvziMUADlyWBg=="
    }
  }
}
