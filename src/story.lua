local story = {}

function story:start(engine)
  engine:load('01.lua')

  engine:animate('romata', 'play')
  engine:playSound('gebet', false, function() engine:stopAnimate('romata') end)

  engine:showMessage('Du befindest dich auf einer sturmgebeutelten Insel. Wo ist denn nur der Met?')
end

return story
