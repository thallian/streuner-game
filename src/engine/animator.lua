local anim8 = require 'engine/libs/anim8'

Animator = class('Animator')

function Animator:init(fullWidth, fullHeight)
  self.animationGrid = anim8.newGrid(Global.tilewidth, Global.tileheight, fullWidth, fullHeight)

  self.walk_up = anim8.newAnimation(self.animationGrid('1-9', 1), 0.1)
  self.walk_left = anim8.newAnimation(self.animationGrid('1-9', 2), 0.1)
  self.walk_down = anim8.newAnimation(self.animationGrid('1-9', 3), 0.1)
  self.walk_right = anim8.newAnimation(self.animationGrid('1-9', 4), 0.1)
  self.play = anim8.newAnimation(self.animationGrid('1-9', 5), 0.1)
end

return Animator
