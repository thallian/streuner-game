local sources = {}

-- check for sources that finished playing and remove them
function love.audio.update()
  local remove = {}
  for _,s in pairs(sources) do
    if s.audio:isStopped() then
      if s.finishedFunc then s.finishedFunc() end

      remove[#remove + 1] = s
     end
   end

  for i,s in ipairs(remove) do
    sources[s] = nil
  end
end

-- overwrite love.audio.play to create and register source if needed
local play = love.audio.play
function love.audio.play(what, how, loop, finishedFunc)
  local src = {}
  if type(what) ~= 'userdata' or not what:typeOf('Source') then
    src.audio = love.audio.newSource(what, how)
    src.finishedFunc = finishedFunc
    src.audio:setLooping(loop or false)
  end

  play(src.audio)
  sources[src] = src

  return src
end

local stop = love.audio.stop
function love.audio.stop(src)
  if not src then return end
  stop(src.audio)
  sources[src] = nil
end
