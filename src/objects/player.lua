return {
  spritesheet = 'assets/characters/player.png',
  pose = 'walk_up',
  controller = 'gridwalker',
  relevantInputs = {
    'up',
    'down',
    'right',
    'left'
  }
}
