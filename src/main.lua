local Engine = require 'engine'

function love.load(arg)
    if arg[#arg] == '-debug' then require('mobdebug').start() end -- debugging in ZeroBraineStudio

    engine = Engine()
end

function love.update(dt)
    engine:update(dt)
end

function love.keyreleased(key)
    engine:checkObjectAnimation(key)
end

function love.draw()
    engine:draw()
end
